import React, {Component} from 'react';
import './footer.component.scss';

class Footer extends Component {
  render() {
    return (
    <footer className="exchg ui inverted vertical footer segment">
        <div className="ui stackable centered grid">
            <div className="ui three wide column">
                <h3>Footer Title 1</h3>
                <nav>
                    <a href="#footer-link1">Footer Link 1.1</a><br/>
                    <a href="#footer-link2">Footer Link 1.2</a><br/>
                    <a href="#footer-link3">Footer Link 1.3</a><br/>
                    <a href="#footer-link4">Footer Link 1.4</a><br/>
                </nav>
            </div>

            <div className="ui three wide column">
                <h3>Footer Title 2</h3>
                <nav>
                    <a href="#footer-link1">Footer Link 2.1</a><br/>
                    <a href="#footer-link2">Footer Link 2.2</a><br/>
                    <a href="#footer-link3">Footer Link 2.3</a><br/>
                    <a href="#footer-link4">Footer Link 2.4</a><br/>
                </nav>
            </div>

            <div className="ui three wide column">
                <h3>Footer Title 3</h3>
                <nav>
                    <a href="#footer-link1">Footer Link 3.1</a><br/>
                    <a href="#footer-link2">Footer Link 3.2</a><br/>
                    <a href="#footer-link3">Footer Link 3.3</a><br/>
                    <a href="#footer-link4">Footer Link 3.4</a><br/>
                </nav>
            </div>

            <div className="ui three wide column">
                <h3>Footer Title 4</h3>
                <nav>
                    <a href="#footer-link1">
                        <i className="fab fa-facebook-square"></i>
                    </a>
                    <a href="#footer-link2">
                        <i className="fab fa-google-plus-square"></i>
                    </a>
                    <a href="#footer-link3">
                        <i className="fab fa-twitter-square"></i>
                    </a>
                </nav>
            </div>
        </div>
    </footer>
    );
  }
}

export default Footer;
