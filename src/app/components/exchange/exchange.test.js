import axios from "axios";
/**
 * LA API SE DEBERIA INCLUIR EN VARIABLES DE AMBIENTE.
 * OBTENERLAS CON PROCESS ENV, ESTABLECERLAS COMO VARIABLES GLOBALES EN WEBPACK CON DEFINEPLUGIN / .ENV / ETC..
 * PARA DSP PODER UTILIZARLAS EN EL PROYECTO..
*/
const exchangeAPI = 'http://data.fixer.io/api/latest?access_key=33b23d6e01efe285daf21f65e1124757';

it('EXCHANGE -- Functionality is working right ?', async function() {
    const config = {
        method: "GET",
        url: exchangeAPI,
    };
    let data = null;
    await axios(config)
            .then((response) => {
                data = response.data;
            });
    expect(data).not.toBe(null);
    expect(data.rates['USD']).not.toBe(null);
    expect(1 * data.rates['USD']).toBeGreaterThan(0);
  });
  

/* INCOMPLETO, TEST DE RENDERIZACION Y COMPORTAMIENTO DE COMPONENTE.

//import React from 'react';
//import { Provider } from "react-redux";
//import Adapter from 'enzyme-adapter-react-16';
//import { mount, configure } from 'enzyme';
//import Exchange from './exchange.component';
//import initialState from '../../store/initialState';
//import configureStore from "../../store/store.config";
//configure({ adapter: new Adapter() });
//const store = configureStore({...initialState, rates:{rates:{ USD: 2}, base: 'EUR', to: 'USD' }});

it('EXCHANGE -- Component Working Right ?', async function() {

  const wrapper = mount(
  <Provider store={store}>
      <Exchange />
  </Provider>);

  const exchgFrom = wrapper.find('input[id="exchangeFrom"]');
  const exchgTo = wrapper.find('input[id="exchangeTo"]');
  const reqBtn = wrapper.find('#submitExchangeRequest');

  wrapper.find('input').at(0).simulate('change', {target: {value: '1'}});
  reqBtn.simulate('click');
  await tick();

  expect(wrapper.find('input')).toHaveLength(2);
  expect(exchgFrom.getDOMNode().value).toBe('€ 1');
  expect(exchgTo.getDOMNode().value).toBe('$ 2');
});

function tick() {
    return new Promise(resolve => {
        setTimeout(resolve, 500);
    })
}
*/
