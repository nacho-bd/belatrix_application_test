import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CurrencyFormat from 'react-currency-format';
import * as ratesActions from '../../store/actions/rates.actions';
import currency from '../../const/currency.const';
import './exchange.component.scss';

const _doExchangeCalc = (rates, exchangeFrom) => {
    return exchangeFrom * rates.rates[rates.to];
}

class Exchange extends Component {
    constructor(){
        super();
        this.state = {
            exchangeFrom: '',
            exchangeTo: '',
            errorMsg: undefined
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (
            prevState.requestMade && 
            prevState.exchangeFrom !== '' &&
            Object.keys(nextProps.rates.rates).length > 0) {
            return {
                requestMade: false,
                exchangeTo: _doExchangeCalc(nextProps.rates, prevState.exchangeFrom)
            };
        }
        return null;
    }
    componentDidMount() {
        const {ratesActions} = this.props;
        this.exchgRatesService = setInterval(() => {
            this.setState({
                requestMade:true
            });
            ratesActions._getExchangeValues();
        }, 600000);
    }
       
    componentWillUnmount() {
        if(this.exchgRatesService){
            clearInterval(this.exchgRatesService);
            this.exchgRatesService = null;
        }
    } 
    _getExchangeValues = () => {
        const {ratesActions, rates} = this.props;
        if(this.state.exchangeFrom !== ''){
            this.setState({
                requestMade:true,
                errorMsg:undefined
            });
            ratesActions._getExchangeValues();
        }else{
            this.setState({
                errorMsg: `Please complete the value in ${rates.base} that you want to convert to ${rates.to}`
            })
        }
    }
    _exchangeFromValueChange = (values) => {
        const {value} = values;
        this.setState({
            exchangeFrom: value,
            exchangeTo: ''
        });
    }
    render() {
        const { rates } = this.props;
        return (
            <section className="exchg ui header text container">
                {
                    this.state.errorMsg && <div className="ui ignored error message">
                        <span>
                            {this.state.errorMsg}
                        </span>
                    </div>
                }
                <div className="ui two column very relaxed stackable grid">
                    <div className="middle aligned column">
                        <div className="field">
                            <label>{this.state.exchangeFrom ? rates.base: '  '} </label>
                            <div className="ui fluid input">
                                <CurrencyFormat  id="exchangeFrom"
                                                value={ this.state.exchangeFrom } 
                                                allowNegative={ false }
                                                onValueChange={ this._exchangeFromValueChange }
                                                placeholder={rates.base} 
                                                type="text" 
                                                thousandSeparator="," 
                                                decimalSeparator="." 
                                                decimalScale={4} 
                                                prefix={`${currency[rates.base].symbol} `} />
                            </div> 
                        </div>
                    </div>
                    <div className="middle aligned column">
                        <div className="field">
                            <label> {this.state.exchangeTo ? rates.to : '  '}</label>
                            <div className="ui fluid input">
                                <CurrencyFormat id="exchangeTo"
                                                value={ this.state.exchangeTo } 
                                                placeholder={rates.to} 
                                                className="ui input" 
                                                onKeyDown={(e)=>{e.preventDefault();}} 
                                                type="text" thousandSeparator="," 
                                                decimalSeparator="." 
                                                decimalScale={4} 
                                                prefix={`${currency[rates.to].symbol} `} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ui one column very relaxed stackable grid">
                    <div className="middle center aligned column"> 
                        <button id="submitExchangeRequest" onClick={ this._getExchangeValues }className="ui large wide primary button">calculate</button>
                    </div>
                </div>
            </section>
        );
    }
}

Exchange.propTypes = {
    currency: PropTypes.object,
    ratesActions: PropTypes.object
};

function mapStateToProps(state) {
    return {
        rates: state.rates,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        ratesActions: bindActionCreators(ratesActions, dispatch)
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Exchange);
