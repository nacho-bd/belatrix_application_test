import React, { Component } from 'react';
import { Provider } from "react-redux";
import Header from './header/header.component';
import Footer from './footer/footer.component';
import Exchange from './exchange/exchange.component';
import {  BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import initialState from '../store/initialState';
import configureStore from "../store/store.config";

/**
 * Dado que es una empresa en expansión, dejo el ruteado hecho para posibles futuros cambios
 */

const store = configureStore(initialState);
class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Header/>
            <Switch>
              <Route path="/home" component={Exchange}/>
              <Redirect to="/home"/>
            </Switch>
            <Footer/>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
