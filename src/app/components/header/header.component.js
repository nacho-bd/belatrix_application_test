import React, {Component} from 'react';
import './header.component.scss';
import logo from '../../assets/svg/logo.svg';

class Header extends Component {
  render() {
    return (
    <header className="exchg ui vertical masthead center aligned segment">
        <img src={logo} className="header-logo" alt="logo" />
        <div className="ui centered grid">
          <div className="center aligned column">
            <nav className="ui large secondary compact menu">
                <a className="item" href="#link1">Link 1</a>
                <a className="item" href="#link2">Link 2</a>
                <a className="item" href="#link3">Link 3</a>
                <a className="item" href="#link4">Link 4</a>
            </nav>
          </div>
        </div>
    </header>
    );
  }
}

export default Header;
