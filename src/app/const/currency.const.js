const currency = {
    EUR:{
        symbol:'€'
    },
    USD:{
        symbol:'$'
    }
}

export default currency;
