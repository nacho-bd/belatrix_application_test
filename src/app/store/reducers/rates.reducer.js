import * as ratesActionsTypes from "../actions/rates.types";
import initialState from "../initialState";

export default function ratesReducer(state = initialState.rates, action) {

  switch (action.type) {
    case ratesActionsTypes.GET_CURRENCY_EXCHANGE_RATES_SUCCESS: {
      return {
        ...state,
        rates: action.data.rates,
        base: action.data.base,
      };
    }
    case ratesActionsTypes.GET_CURRENCY_EXCHANGE_RATES_FAIL: {
      return {
        ...state,
        rates: [],
        base: undefined
      };
    }
    default:
      return state;
  }
}