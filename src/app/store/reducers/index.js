import {combineReducers} from 'redux';
import rates from './rates.reducer';

const rootReducer = combineReducers({
    rates
});

export default rootReducer;