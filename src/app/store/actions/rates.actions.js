import axios from "axios";
import * as ratesActionTypes from "./rates.types";
/**
 * LA API SE DEBERIA INCLUIR EN VARIABLES DE AMBIENTE.
 * OBTENERLAS CON PROCESS ENV, ESTABLECERLAS COMO VARIABLES GLOBALES EN WEBPACK CON DEFINEPLUGIN / .ENV / ETC..
 * PARA DSP PODER UTILIZARLAS EN EL PROYECTO..
*/
const exchangeAPI = 'http://data.fixer.io/api/latest?access_key=33b23d6e01efe285daf21f65e1124757';

function dispatchState(type, response, params = {}) {
    return {
        type: type,
        data: response && response.data ? response.data : {}
    };
}

export function _getExchangeValues() {
    const config = {
        method: "GET",
        url: exchangeAPI,
    };
    return dispatch => {
        dispatch({
            type: ratesActionTypes.GET_CURRENCY_EXCHANGE_RATES_REQUEST
        });
        return axios(config)
            .then((response) => {
                dispatch(dispatchState(ratesActionTypes.GET_CURRENCY_EXCHANGE_RATES_SUCCESS, response));
            })
            .catch(error => {
                dispatch(dispatchState(ratesActionTypes.GET_CURRENCY_EXCHANGE_RATES_FAIL, error.response));
            });
    };
}