import rates from './rates.state';

const initialState = {
    rates: rates
}

export default initialState;