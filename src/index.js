import React from 'react';
import ReactDOM from 'react-dom';
//import './app/styles/index.scss';
import App from './app/components/app.component';
import * as serviceWorker from './serviceWorker';

const bootstrap = document.createElement('div');
document.body.appendChild(bootstrap);
ReactDOM.render(<App />, bootstrap);
serviceWorker.unregister();
